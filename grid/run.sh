#!/usr/bin/env bash

if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
    set -eu
fi

echo "Adding local directory to LD_LIBRARY_PATH"
export LD_LIBRARY_PATH=$(pwd):${LD_LIBRARY_PATH}

PARENT=$(pwd)

cd build

IFS=',' read -a INPUTS <<< "$1"

# if this grid node uses local datasets we need to copy these too
for INPUT in ${INPUTS[*]}; do
    if [[ -f ${PARENT}/${INPUT} ]]; then
        ln -f -s ${PARENT}/${INPUT}
    fi
done

echo "RUNNING ON ${INPUTS[*]}"

./**/bin/dump-single-btag ${INPUTS[*]}

echo "moving output file to parent directory"

mv output.h5 output.root $PARENT

echo 'DONE'
