#include "TrackSelector.hh"
#include "xAODJet/Jet.h"

TrackSelector::TrackSelector(TrackSelectorConfig config):
  m_track_associator("BTagTrackToJetAssociator"),
  m_track_selector("InDetTrackSelectionTool", "Loose"),
  m_track_select_cfg(config)
{
  if (!m_track_selector.initialize()) {
    throw std::logic_error("can't initialize track seletor");
  }
}

TrackSelector::Tracks TrackSelector::get_tracks(const xAOD::Jet& jet) const
{
  const xAOD::BTagging *btagging = jet.btagging();
  std::vector<const xAOD::TrackParticle*> tracks;
  for (const auto &link : m_track_associator(*btagging)) {
    if(link.isValid()) {
      const xAOD::TrackParticle *tp = *link;
      if (m_track_selector.accept(tp) && passed_cuts(*tp)) {
        tracks.push_back(tp);
      }
    } else {
      throw std::logic_error("invalid track link");
    }
  }
  return tracks;
}

bool TrackSelector::passed_cuts(const xAOD::TrackParticle& tp) const
{
  static SG::AuxElement::ConstAccessor<float> d0("btagIp_d0");
  static SG::AuxElement::ConstAccessor<float> z0("btagIp_z0SinTheta");
  float pt = tp.pt();
  return ((std::abs(d0(tp)) < m_track_select_cfg.d0_maximum) && 
          (std::abs(z0(tp)) < m_track_select_cfg.z0_maximum) && 
          (pt > m_track_select_cfg.pt_minumum));
}
