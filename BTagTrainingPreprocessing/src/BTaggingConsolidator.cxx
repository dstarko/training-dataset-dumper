#include "BTaggingConsolidator.hh"

BTaggingConsolidator::BTaggingConsolidator(
  const std::map<std::string,std::pair<std::regex,std::string>>& jc_regex,
  const std::vector<std::string>& vars_to_copy) {
  for (const auto& jc_re: jc_regex) {
    for (const std::string& var: vars_to_copy) {
      std::string jc = jc_re.first;
      std::regex re = jc_re.second.first;
      std::string fmt = jc_re.second.second;
      std::string targ_name = regex_replace(var, re, fmt);
      if (targ_name == var) {
        throw std::runtime_error("regex failed for " + var);
      }
      Decorators deco{var, targ_name};
      m_decorators[jc].push_back(deco);
    }
  }
}

void BTaggingConsolidator::consolidate(const xAOD::JetContainer* targ,
                                       const InJets& src) const {
  for (const auto& jetpair: src) {
    const auto& decorators = m_decorators.at(jetpair.first);
    for (const Decorators& deco: decorators) {
      const xAOD::JetContainer* source_jets = jetpair.second;
      if (source_jets->size() != targ->size()) {
        throw std::logic_error("size of containers doesn't match");
      }
      for (size_t iii = 0; iii < source_jets->size(); iii++) {
        const xAOD::BTagging* src_b = source_jets->at(iii)->btagging();
        const xAOD::BTagging* targ_b = targ->at(iii)->btagging();
        deco.target(*targ_b) = deco.source(*src_b);
      }
    }
  }
}

